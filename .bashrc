#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export EDITOR=vim VISUAL=vim

# nethack options
NETHACKOPTIONS="color,hilite_pet,lit_corridor,DECgraphics,boulder:0,!autopickup,msg_window:f"
PS1="[\e[0;36m\t \w\e[m]\n[\u@\h]$ "
PATH="~/bin/:$PATH"

# history stuff
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
export HISTCONTROL=ignoreboth
export HISTIGNORE='history*'

shopt -s checkwinsize

alias ls='ls --color=auto'

PATH="$HOME/.local/bin:$PATH"

export TZ='US/Central'
