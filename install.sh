#!/bin/sh

## Ensure the Unison profile is in place
unison -auto ~/.unison/dotfiles.prf $(dirname $0)/.unison/dotfiles.prf

## Synchronize
unison dotfiles

## Vim housekeeping
# YouCompleteMe has native code too, so install it first
vim '+PluginInstall "Valloric/YouCompleteMe"' +qall
# install.sh does a relative `find`, so we need to be in its directory
pushd ~/.vim/bundle/YouCompleteMe/
./install.sh --clang-completer --system-libclang || return
popd

# Install everything and update existing plugins
vim +PluginInstall +PluginUpdate +qall