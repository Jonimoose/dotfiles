set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
if has("win32")
    set rtp+=~/vimfiles/bundle/Vundle.vim
    call vundle#begin('~/vimfiles/bundle/')
else
    set rtp+=~/.vim/bundle/Vundle
    call vundle#begin()
endif
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'Valloric/YouCompleteMe'

Plugin 'bling/vim-airline'

Plugin 'Lokaltog/vim-easymotion'


" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


set hidden
set history=200
set scrolloff=4
set vb
set ruler
set showcmd
set incsearch
set hlsearch

if has("multi_byte_encoding")
    set encoding=utf-8
endif

" UI things
if !has("gui_running")
    set t_Co=256
    let g:airline_theme="kolor"
endif
set guifont=DejaVu_Sans_Mono:h10

colors slate

" relativenumber in normal mode, otherwise just number
set relativenumber nonumber
autocmd InsertEnter * :set number norelativenumber
autocmd InsertLeave * :set relativenumber nonumber
" and a quick-change mapping
function! NumberModeToggle()
    if(&relativenumber == 1)
        set norelativenumber
        set number
    else
        set nonumber
        set relativenumber
    endif
endfunc
nnoremap <C-n> :call NumberModeToggle()<cr>

set list listchars=tab:>-
set scrolloff=4
set laststatus=2


" Stolen from Peter, have to look up what exactly they do.
let g:ycm_autoclose_preview_window_after_insertion=1
" let g:ycm_extra_conf_globlist = ['~/projects/*']


" Indentation (4 spaces mostly)
"set ai smartindent
filetype indent on
set et
set ts=4 sw=4 sts=4

set fo+=c
set spelllang=en_us

" allow backspacing over everything in insert mode
set backspace=indent,eol,start


" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif

syntax on


" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif


