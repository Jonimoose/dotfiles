" Vim syntax file
" Language: PowerMILL Macro File
" Maintainer: Jon Sturm
" Latest Revision: 10 September 2015

if exists("b:current_syntax")
  finish
endif

syn case ignore

"Keywords
syn keyword	pmStatement break return continue
syn keyword	pmLabel case default
syn keyword	pmConditional if else switch
syn keyword	pmRepeat while foreach do
syn keyword pmOperator and in is not or

syn keyword pmConstants FALSE TRUE CRLF PI E YES

syn keyword pmSystem INPUT ERROR QUERY PRINT MESSAGE

syn keyword pmBuiltin abs acos active active_folder add_first add_last angle apparent_angle asin
syn keyword pmBuiltin atan atan2 azimuth basename bool closest_point_on_toolpath compare components
syn keyword pmBuiltin convert cos desc deselect_components dirname dir_exists elevation entity
syn keyword pmBuiltin entity_exists error evaluate exp extract file_exists fill filter folder
syn keyword pmBuiltin folder_exists gauge_length geometry_equal get_folders get_typename home int
syn keyword pmBuiltin intersection is_empty join lcase length level_entity limits 
syn keyword pmBuiltin limits_workplane_rel list_files list_nctools ln local_time locked log ltrim 
syn keyword pmBuiltin machine machine_axis macro_path make_sequence max mean member min 
syn keyword pmBuiltin model_components_thicknessset nctoolpath_fixtureoffset new_entity_name 
syn keyword pmBuiltin next_in_sequence normal number_nctoolpaths number_nctools number_selected 
syn keyword pmBuiltin parallel pathname plugin_value point_add_parameter point_parameters 
syn keyword pmBuiltin point_remove_parameter point_remove_parameters position project_pathname pwd 
syn keyword pmBuiltin real remove remove_duplicates remove_first remove_last replace reverse round 
syn keyword pmBuiltin rtrim sanitize segments segment_create_point_at_distance segment_get_length
syn keyword pmBuiltin segment_get_point_at_distance segment_point_count select select_components
syn keyword pmBuiltin set_point set_union set_vector set_workplane sign sin size sort sqrt string
syn keyword pmBuiltin substring subtract tan time time_formatted time_to_string tokens
syn keyword pmBuiltin toolpath_component_count toolpath_cut_limits to_work to_world
syn keyword pmBuiltin translate trim ucase unit user_id utc_time values

syn keyword pmType INT REAL STRING BOOL ENTITY OBJECT FUNCTION

syn region pmBlock start="{" end="}" transparent fold

syn region pmString start=+[uU]\=\z(['"]\)+ end="\z1" skip="\\\\\|\\\z1" contains=@Spell

syn region pmComment start="\#" end="$" keepend

syn region pmComment start="//" skip="\\$" end="$" keepend contains=@Spell display

syn match pmIdentifier "$\h\w*"


" syn match pmOperator "[<>+\*^/\\=-]""

" syn keyword pmMainOperator EDIT DELETE CREATE ACTIVATE DEACTIVATE IMPORT DRAW UNDRAW TRANSFORM


let b:current_syntax = "pmill"

hi def link pmStatement Statement
hi def link pmLabel Label
hi def link pmConditional Conditional
hi def link pmRepeat Repeat
hi def link pmType Type
hi def link pmOperator Operator
hi def link pmComment Comment
hi def link pmSystem Function
hi def link pmBuiltin Function
hi def link pmMainOperator Operator
hi def link pmConstants Constant
hi def link pmIdentifier Identifier

hi def link pmString String

