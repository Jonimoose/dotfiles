autoload -U compinit promptinit

compinit
zstyle ':completion:*' menu select
setopt completealiases

HISTFILE=~/.history
HISTSIZE=1000
SAVEHIST=1000
setopt inc_append_history
setopt extended_history
setopt hist_expire_dups_first
setopt hist_reduce_blanks
setopt share_history

promptinit
prompt adam2

autoload -Uz vcs_info
zstyle ':vcs_info:*' enable hg git
zstyle ':vcs_info:hg*:*' check-for-changes true
zstyle ':vcs_info:hg*' actionformats "(%s|%a)[%i%u %b %m]"
precmd() { vcs_info }
RPROMPT='${vcs_info_msg_0_}'

bindkey -v
function zle-line-init zle-keymap-select {
    VIM_PROMPT="%{$fg_bold[yellow]%} [% NORMAL]%  %{$reset_color%}"
    RPROMPT="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/}${vcs_info_msg_0_}"
    zle reset-prompt
}
zle -N zle-line-init
zle -N zle-keymap-select
export KEYTIMEOUT=1

alias ag="ag --pager 'less -R'"
alias ls='ls --color=auto'
alias less='less -R'
export PATH="$HOME/bin:$PATH"
export EDITOR=vim
export VISUAL=vim

source ~/.profile
